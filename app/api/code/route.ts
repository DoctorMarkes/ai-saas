import { auth } from '@clerk/nextjs'
import { NextResponse } from 'next/server'

import OpenAI from 'openai'

const openai = new OpenAI()

const instructionMessage: OpenAI.ChatCompletionSystemMessageParam = {
  role: 'system',
  content:
    'You are a code generator. You must answer only in markdown code snippets. Use code comments for explanations.',
}

export async function POST(req: Request) {
  try {
    const { userId } = auth()
    const body = await req.json()
    const { messages } = body

    if (!userId) {
      return new NextResponse('Unauthorized', { status: 401 })
    }

    if (!messages) {
      return new NextResponse('Messages are required', { status: 400 })
    }

    const completion = await openai.chat.completions.create({
      model: 'gpt-4-turbo-preview',
      messages: [instructionMessage, ...messages],
    })

    return NextResponse.json(completion.choices[0].message)
  } catch (error) {
    console.log('[CONVERSATION_ERROR]', error)
    return new NextResponse('Internal Error', { status: 500 })
  }
}
